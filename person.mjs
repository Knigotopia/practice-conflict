export default class Person {
    constructor(name,surname) {
        this.name = name;
        this.surname = surname;
    }

    say(phrase) {
        return `${this.name + ' ' + this.surname} says ${phrase}`;
    }

    question(str) {
      return `${this.name} asks ${str}`;
    }
}
